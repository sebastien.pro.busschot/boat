from flask import jsonify
import sys

class Model:

    def map_list_objects(self, list, toJson = False):
        map_to_return = []
        for i in list:
            map_to_return.append(self.list_to_map(i))
        
        return self.select_format(map_to_return, toJson)

    
    def select_format(self, data, toJson):
        if toJson:
            return jsonify(data)
        else:
            return data

    def list_to_map(self, list_data, toJson = False):
        map_to_return = []

        newDict = {}
        for i in range(len(self.attr)):
            newDict[self.attr[i]] = list_data[i]

        map_to_return.append(newDict)

        return map_to_return