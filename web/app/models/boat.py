import sys
from flask import jsonify
from models.model import Model

class Boat(Model):
    id = 0
    description = ""
    direction = 0
    target_direction = 0
    latitude = 0.0
    longitude = 0.0
    lights = 0
    water_inboard = 0
    speed = 0
    rudder_direction = 0
    app_token = ""
    autopilot = 0

    attr = ("id", "description", "direction", "target_direction", "latitude", "longitude", "lights", "water_inboard", "speed", "rudder_direction", "app_token", "autopilot")

    def hydrate(self, id, description, direction, target_direction, latitude, longitude, lights, water_inboard, speed, rudder_direction, app_token, autopilot):
        self.id = id
        self.description = description
        self.direction = direction
        self.target_direction = target_direction
        self.latitude = latitude
        self.longitude = longitude
        self.lights = lights
        self.water_inboard = water_inboard
        self.speed = speed
        self.rudder_direction = rudder_direction
        self.app_token = app_token
        self.autopilot = autopilot

    
