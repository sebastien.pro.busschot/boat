import sys
from flask import jsonify
from models.model import Model
from datetime import Datetime
from models.user import User

class Log(Model):
    id = 0
    boat_json_state = {}
    datetime = Datetime()
    user = User()

    attr = ("id", "boat_json_state", "datetime", "user")

    def hydrate(self, id, boat_json_state, datetime, user):
        self.id = id
        self.boat_json_state = boat_json_state
        self.datetime = datetime
        self.user = user