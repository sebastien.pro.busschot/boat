import sys
from flask import jsonify
from models.model import Model
from models.boat import Boat
from enum import Enum

class Motor_state(Enum):
    front = "front"
    back = "back"
    stop = "stop"
    neutral = "neutral"

class Motor(Model):
    id = 0
    power = 0
    state = Motor_state.neutral
    description = ""
    boat = Boat()

    attr = ("id", "power", "state", "description", "boat")

    def hydrate(self, id, power, state, description, boat, longitude, lights, water_inboard, speed, rudder_direction, app_token, autopilot):
        self.id = id
        self.power = power
        self.state = state
        self.description = description
        self.boat = boat

    
