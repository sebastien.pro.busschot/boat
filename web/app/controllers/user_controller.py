from controllers.controller import Controller

class User_controller(Controller):

    def getUsers(self):
        return "Users"

    def getUser(self, id):
        return "User {}".format(id)

    def postUser(self, data):
        return data

    def putUser(self, id, data):
        return "update User"

    def deleteUser(self, id):
        return "delete User"