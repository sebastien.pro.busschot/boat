from flask import Flask, render_template, jsonify
from models.boat import Boat
from controllers.controller import Controller
import sys

class Boat_controller(Controller):

    def getBoats(self):
        mycursor = self.mydb.cursor()
        mycursor.execute("SELECT * FROM boats")
        myresult = mycursor.fetchall()
        
        boat = Boat()
        boats = boat.map_list_objects(myresult, True)
        
        return boats

    def getBoat(self, id):
        
        return "boat {}".format(id)

    def postBoat(self, data):
        return data
        #return "new boat"

    def putBoat(self, id, data):
        
        return "update boat"

    def deleteBoat(self, id):
        
        return "delete boat"

    