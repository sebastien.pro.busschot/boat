import mysql.connector
from flask import Flask, render_template, request
from controllers.boat_controller import Boat_controller
from controllers.user_controller import User_controller
from controllers.auth_controller import Auth_controller
import sys

# Print pour debug
# print(i, file=sys.stderr)

app = Flask(__name__)

mydb = mysql.connector.connect(
  host="database",
  user="root",
  password="1C_Udp8q_H",
  database="rc_boat"
)

@app.route('/', methods=['GET'])
def index():
  return render_template("login.html")

@app.route('/api/login', methods=['POST'])
def login():
  auth_c = Auth_controller(mydb)
  return auth_c.login(request.form)

# Boat routing
@app.route('/api/boats', methods=['GET'])
def getBoats():
  boat_c = Boat_controller(mydb)
  return boat_c.getBoats()

@app.route('/api/boat/<int:id>', methods=['GET'])
def getBoat(id):
  boat_c = Boat_controller(mydb)
  return boat_c.getBoat(id)

@app.route('/api/boat', methods=['POST'])
def createBoat():
  boat_c = Boat_controller(mydb)
  return boat_c.postBoat(request.get_json())

@app.route('/api/boat/<int:id>', methods=['PUT'])
def updateBoat(id):
  boat_c = Boat_controller(mydb)
  return boat_c.putBoat(id, request.get_json())

@app.route('/api/boat/<int:id>', methods=['DELETE'])
def deleteBoat(id):
  boat_c = Boat_controller(mydb)
  return boat_c.deleteBoat(id)

# User routing
@app.route('/api/users', methods=['GET'])
def getUsers():
  user_c = User_controller(mydb)
  return user_c.getUsers()

@app.route('/api/user/<int:id>', methods=['GET'])
def getUser(id):
  user_c = User_controller(mydb)
  return user_c.getUser(id)

@app.route('/api/user', methods=['POST'])
def createUser():
  user_c = User_controller(mydb)
  return user_c.postUser(request.get_json())

@app.route('/api/user/<int:id>', methods=['UPDATE'])
def updateUser(id):
  user_c = User_controller(mydb)
  return user_c.updateUser(id, request.get_json())

@app.route('/api/user/<int:id>', methods=['DELETE'])
def deleteUser(id):
  user_c = User_controller(mydb)
  return user_c.deleteUser(id)


app.run(debug=True)