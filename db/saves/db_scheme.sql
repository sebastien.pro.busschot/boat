-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema rc_boat
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema rc_boat
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `rc_boat` ;
USE `rc_boat` ;

-- -----------------------------------------------------
-- Table `rc_boat`.`boats`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `rc_boat`.`boats` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `description` VARCHAR(100) NULL,
  `direction` INT(3) NOT NULL,
  `target_direction` INT(3) NOT NULL,
  `latitude` FLOAT NOT NULL,
  `longitude` FLOAT NOT NULL,
  `lights` TINYINT NOT NULL,
  `water_inboard` TINYINT NOT NULL,
  `speed` INT(3) NOT NULL,
  `rudder_direction` INT(3) NOT NULL,
  `app_token` VARCHAR(45) NOT NULL,
  `autopilot` TINYINT NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC) VISIBLE,
  UNIQUE INDEX `app_token_UNIQUE` (`app_token` ASC) VISIBLE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `rc_boat`.`checkpoints`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `rc_boat`.`checkpoints` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `boats_id` INT NOT NULL,
  `latitude` FLOAT NOT NULL,
  `longitude` FLOAT NOT NULL,
  PRIMARY KEY (`id`, `boats_id`),
  INDEX `fk_checkpoints_boats1_idx` (`boats_id` ASC) VISIBLE,
  CONSTRAINT `fk_checkpoints_boats1`
    FOREIGN KEY (`boats_id`)
    REFERENCES `rc_boat`.`boats` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `rc_boat`.`motors`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `rc_boat`.`motors` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `power` INT(3) NOT NULL,
  `state` ENUM("front", "back", "stop", "neutral") NOT NULL,
  `description` VARCHAR(10) NOT NULL,
  `boats_id` INT NOT NULL,
  PRIMARY KEY (`id`, `boats_id`),
  INDEX `fk_motors_boats1_idx` (`boats_id` ASC) VISIBLE,
  CONSTRAINT `fk_motors_boats1`
    FOREIGN KEY (`boats_id`)
    REFERENCES `rc_boat`.`boats` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `rc_boat`.`user`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `rc_boat`.`user` (
  `id` INT NOT NULL,
  `username` VARCHAR(25) NOT NULL,
  `salt` VARCHAR(255) NOT NULL,
  `hash` VARCHAR(255) NOT NULL,
  `admin` TINYINT NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `username_UNIQUE` (`username` ASC) VISIBLE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `rc_boat`.`logs`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `rc_boat`.`logs` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `boat_json_state` JSON NOT NULL,
  `datetime` DATETIME NOT NULL DEFAULT now(),
  `user_id` INT NOT NULL,
  PRIMARY KEY (`id`, `user_id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC) VISIBLE,
  INDEX `fk_logs_user1_idx` (`user_id` ASC) VISIBLE,
  CONSTRAINT `fk_logs_user1`
    FOREIGN KEY (`user_id`)
    REFERENCES `rc_boat`.`user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
