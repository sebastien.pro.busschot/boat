#!/bin/bash

if [ "$1" == "-i" ]
then
    echo "Import save_rc_boat.sql in docker"
    docker exec -i boat-database-1 sh -c 'exec mysql rc_boat -u root --password=1C_Udp8q_H < /home/mysql/save_rc_boat.sql'
elif [ "$1" == "-d" ]
then
    echo "Dump docker's rc_boat mysql database in save_rc_boat.sql"
    docker exec boat-database-1 sh -c 'exec mysqldump rc_boat -u root --password=1C_Udp8q_H > /home/mysql/save_rc_boat.sql'
elif [ "$1" == "-s" ]
then
    echo "Import rc_boat scheme in docker"
    docker exec boat-database-1 sh -c 'exec mysql -u root --password=1C_Udp8q_H < /home/mysql/db_scheme.sql'
else
    echo -e "-i for import\n-d for dump"
fi