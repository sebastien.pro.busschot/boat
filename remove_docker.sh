#!/bin/bash

docker stop boat-database-1 boat-web-1
docker rm boat-database-1 boat-web-1
docker rmi boat_web mysql

if [ "$1" == "-f" ]
then
    sudo rm -rf db/data/*
    touch db/data/.gitkeep
fi

docker-compose up -d

if [ "$1" == "-f" ]
then
    dump_import.sh -i
fi

if [ "$1" == "--debug" ] || [ "$2" == "--debug" ]
then
    sleep 3
    docker logs boat-web-1 --follow
fi